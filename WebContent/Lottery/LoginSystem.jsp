<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
session.invalidate();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">
	body{
		background-color:#7DB9DE ;
		font-family:Arial, Helvetica, sans-serif;
	}
	form{
		margin: 120px;
		display: flex;
	}
	table{
		background: #fff;
		padding: 30px;
		justify-content: center;
	}
	.title{
		font-size: 30px;
		color: #fff;
		margin: 20px 0;
	}
	
   #login{
	  box-shadow: rgb(54, 150, 209) 2px 2px 3px;
      margin: 0 auto;
      height: 50px;
      text-align: center;
	  border-radius: 5px;
   }
   #login tr td{
	   color: rgb(23, 23, 80);
	   padding: 10px 0;
   }
   .btn{
	   color: #fff;
	   border:1px solid  rgb(25, 73, 104);
	   padding: 10px 15px;
	   border-radius: 5px;
	   background: rgb(25, 73, 104);
   }
   .btn2{
	   color: rgb(25, 73, 104);
	   padding: 10px 15px;
	   border:1px solid  rgb(25, 73, 104);
	   border-radius: 5px;
	   background: #fff;
   }
   .btn:hover, .btn2:hover{
	   opacity: 0.7;
	   cursor: pointer;
   }
</style>
<title>Login System</title>ß
</head>
<body>
	<form action="LotteryImpl.do" method="post">
	    <table id="login">
	      <caption class="title">User Login System</caption>
	      <tr>
	        <td>Name:</td>
		    <td><input type="text" name="userName"/></td> 
		  </tr>
		  <tr>
	        <td>Password:</td>
		    <td><input type="password" name="userPwd"/></td> 
		  </tr>
		  <tr>
	        <td colspan="2">
	          <input class="btn" type="submit" value="Login">
	          <input class="btn2" type="reset" value="Reset">
	        </td>
		  </tr>		
		</table>
	</form>
		<center>${msgErr}<br/></center>
</body>
</html>
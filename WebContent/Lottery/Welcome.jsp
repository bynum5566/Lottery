<!-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="loginCheck.jsp" %>
<%response.setCharacterEncoding ("UTF-8");%> -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome!!</title>


<script language="javascript">
        function numcheck(id,time){
         var re = /^[0-9]+$/;
         if (!re.test(time.value)){
          alert("Integer only please!");
          document.getElementById(aaa).value="";
         }
        }
</script>
<style>
	body{
		background-color:#7DB9DE ;
		font-family:Arial,"Microsoft JhengHei", Helvetica, sans-serif;
	}
    .welcome{
        color: #fff;
        font-size: 32px;
        font-weight: 600;
        text-align: center;
        text-shadow: rgb(6, 53, 105) 2px 2px 2px;
        margin: 0 auto;
        padding: 30px;
        line-height: 50px;
    }
    header{
        width: 100%;
        height: 400px;
        background: url("https://i.imgur.com/hu1833r.jpg");
        background-position: center;
        position: relative;
    }
    .input{
        width: 250px;
        padding: 20px 0;
        border-radius: 5px;
        font-size: 36px;
        text-align: center;
        font-weight: 600;
    }
    .btn{
       width: 250px;
       margin: 20px;
       font-size: 28px;
       font-weight:600;
	   color: #fff;
	   border:1px solid  rgb(25, 73, 104);
	   padding: 20px 10px;
	   border-radius: 5px;
	   background: rgb(25, 73, 104);
    }
    .btn2{
	   color: #fff;
	   border:1px solid  rgb(25, 73, 104);
	   padding: 10px 15px;
	   border-radius: 5px;
	   background: rgb(25, 73, 104);
       position: absolute;
       top:102%;
       right:3px;
       transition: 0.5s;
    }
    .btn:hover, .btn2:hover{
	   opacity: 0.8;
	   cursor: pointer;
   }
   .number{
       width: 70%;
       padding: 10px;
       background: #fff;
       margin: 0 auto;
       border-radius: 5px;
       font-size: 36px;
       font-weight: 600;
       color: #000;
       text-align: center;
   }
</style>
</head>


<body>
     <header>
        <input class="btn2" type="button" value="Log me out" onclick="location.href='LoginSystem.jsp'"/>
    </header>
   <div class="welcome">
        ${msgWel} Love to see you friend!!<br/>
        How many lottery sets you wanna generate today?<br>
        <form action="LotteryImpl.do" method="get">
        <input class="input" type="number" id="aaa" onkeyup="numcheck(this.id,this)"/>
        <br>
        <input class="btn" type="submit" value="Enter"/>
        <br>
        </form>
        Here is the lucky numbers! Wish you good luck!<br>
        <div class="number">
            <p>
               54 , 87 , 222, 888, 32
               <%
               
               %>
            </p>
        </div>
    </div>
</body>
</html>


package tw.Lottery;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface LotteryDAO {
	public boolean createConn();
	boolean chkConn(String user,String pwd);
	public void PermittedSession(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	public void ReturnLoginPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	public void closeConn() throws SQLException;
	public void drawing(HttpServletRequest request, HttpServletResponse response,int sets);
	
	public void invaliateSession(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
//	^它暫時用不到
}

package tw.Lottery;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/Lottery/LotteryImpl.do")
public class LotteryImpl extends HttpServlet implements LotteryDAO {
	private static final long serialVersionUID = 1L;
	private Connection conn;
	private PrintWriter out;
	private RequestDispatcher rd;
	private HttpSession session1;
	public boolean createConn() {
		try{
			InitialContext context1=new InitialContext();
			DataSource ds1 = (DataSource)context1.lookup("java:comp/env/connecting/MSSQL");
			conn =ds1.getConnection();
			return true;
		}catch(Exception e){
			return false;
		}	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {			processAction(request,response);
		} catch (Exception e) {			e.printStackTrace();		}	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			int sets = Integer.parseInt(request.getParameter("Enter"));
			drawing(request, response,sets);		
		} catch (Exception e) {
			e.printStackTrace();		}	}
	private void processAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setContentType("UTF-8");
		out = response.getWriter();
		if(createConn()) {
			String user=request.getParameter("userName");
			String pwd=request.getParameter("userPwd");
			if(chkConn(user,pwd)) {
				PermittedSession(request, response);
			}else {
				ReturnLoginPage(request, response);
			}
			closeConn();
		}else {
			out.write("Connection is NG");
		}
			out.close();	}
	public boolean chkConn(String user, String pwd) {
		try {
		if(user!=null && user.length()!=0 && pwd!=null && pwd.length()!=0 ) {
			String sqlStr="Select * From Users Where userName=? and userPwd=?";
			PreparedStatement preState = conn.prepareStatement(sqlStr);
			preState.setString(1,user);
			preState.setString(2,pwd);
			ResultSet rs = preState.executeQuery();
				if(rs.next()){					  
					rs.close();
					preState.close();
					return true;
				}else{					   
					rs.close();
					preState.close();
					return false;
					}	
		}else {				return false;}	} catch (SQLException e) {
			e.printStackTrace();
		}
		return false; 
	}
		
	public void PermittedSession(HttpServletRequest request, HttpServletResponse response) {
		session1 = request.getSession();
		session1.setAttribute("certifiedSession",true);
		request.setAttribute("msgWel","Let's have fun!!");
		rd=request.getRequestDispatcher("Welcome.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public void ReturnLoginPage(HttpServletRequest request, HttpServletResponse response) {
		rd=request.getRequestDispatcher("LoginSystem.jsp");
		request.setAttribute("msgErr", "<h5><font color=\"#FF0000\">Please Input Correct UserName and Password</font></h5>");
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	}
	public void closeConn() throws SQLException {
		 if(conn!=null) {
			 conn.close();
		 }	}
	
	public void drawing(HttpServletRequest request, HttpServletResponse response,int sets) {
		int[][] LuckN = new int[sets][7];		
		for(int q=0;q<sets;q++) {
	        long[] Num = new long[100000];
	        for (int i=0;i<100000;i++){
	        Num[i] = (long)((Math.random()*42)+1);}
	
	        int[] Count = new int[42];
	        int[] Order = new int[42];
	        for(int k=0;k<42;k++){
	        	Order[k]=k+1;
	            for(long j:Num){
	                if(j==(k+1)){
	                    Count[k]=Count[k]+1;
	                }          }           }        
	        for(int n = 0; n<=41; n++) { 
		        for(int m = n+1; m<=41 ; m++){
		            int temp1=0; int temp2=0;
		            if(Count[n]<Count[m]){
		                temp1=Count[n];
		                temp2=Order[n];
		                Count[n] = Count[m];
		                Order[n] = Order[m];
		                Count[m]=temp1;
		                Order[m]=temp2;
		            }
		        } }   
	//        排序 
	        for (int g=0;g<8;g++) {
	        	LuckN[q][g]=Order[g];
	        }
    	request.setAttribute(Integer.toString(q),LuckN[q]);
		}
	}
	
	public void invaliateSession(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		session1.invalidate();
		ReturnLoginPage(request, response);
	}

}